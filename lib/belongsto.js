var mongoose = require('mongoose');

/*
  setup the model with a relation to a parent
  @param {Object} schema
  @param {Object} options
  @option options[] {String} collection - name of mongo collection to link to
  @option options[] {String} name - association name (optional)
*/
module.exports = function(schema, options){
  // console.log(options);
  // Need at least a collection name
  if (!options.collection) {
    throw new Error('no collection given')
  }

  // If the user doesn't specify the name of the relationship then infer it from the collection
  if (!options.name) {
    options.name = options.collection.charAt(0).toLowerCase() + options.collection.slice(1)
  }

  // Foreign keys have an 'Id' on the end such as userId
  options.foreignKey = options.name + 'Id';
  options.capitalizedName = options.name.charAt(0).toUpperCase() + options.name.slice(1)
  options.required = false;

  // Build the relationship attributes for the schema
  var schemaAdditions = {};

  schemaAdditions[options.foreignKey] = {
    type: mongoose.Schema.ObjectId,
    ref: options.collection,
    required: options.required
  }

  schema.add(schemaAdditions);

  // Associations most likely want an index so do that too!
  var indexAdditions = {};
  indexAdditions[options.foreignKey] = 1;
  schema.index(indexAdditions);

  // add a method to fetch a user
  schema.methods['fetch' + options.capitalizedName] = function(select, callback) {
    if (typeof select == 'function') {
      callback = select;
      select = '';
    }
    var self = this;

    if (options.promise && !callback){
      var deferred = options.promise.defer()
      mongoose.model(options.collection).findById(self.get(options.foreignKey)).select(select).exec(function(err, doc){
        if (err) {
          deferred.reject(err);
        } else {
          deferred.resolve(doc);
        }
      })
      return deferred.promise
    } else {
      mongoose.model(options.collection).findById(this.get(options.foreignKey)).select(select).exec(callback)
    }
  };

  /*
    todo.user // print userId

    Idea: could create a private objectCache which we could add the actual user to on fetch
  */
  schema.virtual(options.name).get(function(){
    return this.get(options.foreignKey);
  });


  /*
    todo.user = {_id: ObjectId(987a0ac09a79e0f097a), name: 'Fred', email: 'fred@flintstones.com'} // print userId

    Idea: could create a private objectCache which this saves to as well as setting the foreign key
  */
  schema.virtual(options.name).set(function(item){
    return this.set(options.foreignKey, item) // mongoose typecasts the object to an objectId
  });

};