var Bluebird = require('bluebird');
var mocha = require('mocha'),
    chai = require('chai'),
    mongoose = require('mongoose'),
    mongooseBelongsto = require('../index'),
    expect = chai.expect,
    UserSchema,
    TodoSchema

// Bluebird.promisify(mongoose);
// Bluebird.promisify(mongooseBelongsto);
/*
  Setup test environment
*/

mongoose.connect('mongodb://localhost/mongoose-belongsto')
mongoose.connection.on('error', function(err){
  console.error('MongoDB error:', err);
  console.error('Make sure a mongoDB server is running and accessible by this application');
});

var UserSchema = new mongoose.Schema({
  name: {type: String},
  email: {type: String}
});

var User = mongoose.model('User', UserSchema);


var TodoSchema = new mongoose.Schema({
  text: {type: String}
});

TodoSchema.plugin(mongooseBelongsto, {collection: 'User', promise: Bluebird});

var Todo = mongoose.model('Todo', TodoSchema);


var TaskSchema = new mongoose.Schema({
  text: {type: String}
});

TaskSchema.plugin(mongooseBelongsto, {collection: 'User', name: 'actor', promise: Bluebird});

var Task = mongoose.model('Task', TaskSchema);


/*
  Usage
*/

var user = new User({name: 'Fred', email: 'fred@flintstones.com'});

var todo = new Todo({text: 'A big item'});
var task = new Task({text: 'A big task'});

describe('Mongoose Belongsto', function(){

  before(function(done){
    user.save(function(err){
      if (err){
        return done(err)
      }
      done();
    });
  });

  after(function(done){
    User.remove(function(){
      Todo.remove(function(){
        done();
      });
    });
  });

  context('When standard plugin is applied to the Todo model', function(){

    before(function(){
      todo.userId = user.id;
    });

    describe('userId', function () {
      it('should create a userId attribute', function(){
        expect(todo.schema.paths).to.have.property('userId');
        expect(todo.userId.toString()).to.equal(user.id.toString());
      });
    });

    describe('fetchUser', function () {
      it('should fetch the user', function (done) {
        todo.fetchUser(function(err, foundUser){
          expect(foundUser.id.toString()).to.equal(user.id.toString())
          done(err)
        });
      });


      it('should fetch only the attributes asked for from the user', function (done) {
        todo.fetchUser('name', function(err, foundUser){
          expect(foundUser.name).to.equal(user.name);
          expect(foundUser.email).to.be.undefined;
          done(err)
        });
      });


      it('should fetch the user using a promise', function (done) {
        todo.fetchUser().then(function(foundUser){
          expect(foundUser.name).to.equal(user.name);
          done()
        });
      });
    });
  });

  context('When renamed plugin is applied to the Tasks model', function(){

    before(function(){
      task.actorId = user.id;
    });

    describe('actorId', function () {
      it('should create a userId attribute', function(){
        expect(task.schema.paths).to.have.property('actorId');
        expect(task.actorId.toString()).to.equal(user.id.toString());
      });
    });

    describe('fetchActor', function () {
      it('should fetch the user', function (done) {
        task.fetchActor(function(err, foundUser){
          expect(foundUser.id.toString()).to.equal(user.id.toString())
          done(err)
        });
      });


      it('should fetch only the attributes asked for from the user', function (done) {
        task.fetchActor('name', function(err, foundUser){
          expect(foundUser.name).to.equal(user.name);
          expect(foundUser.email).to.be.undefined;
          done(err)
        });
      });


      it('should fetch the user using a promise', function (done) {
        task.fetchActor().then(function(foundUser){
          expect(foundUser.name).to.equal(user.name);
          done()
        });
      });
    });
  });
});