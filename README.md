# Mongoose Belongsto

Lightweight plugin to add a foreign key, index and getter/setter method

## Setup

To set up a relationship in which a Todo belongs to a User.

```javascript
    var mongoose = require('mongoose');
    var belongsTo = require('mongoose-belongsto');

    // User model
    var UserSchema = new mongoose.Schema({name: String, email: String});
    
    mongoose.model('User', UserSchema);

    // Todo model
    var TodoSchema = new mongoose.Schema({todo: String})

    TodoSchema.plugin(belongsTo, {collection: 'User', name: 'owner'})

    mongoose.model('Todo', TodoSchema);
```

## Create models

```javascript
    var user, todo
    
    user = new mongoose.model('User', {name: 'Fred', email: 'fred@flintstones.com'});
    
    user.save(function(err){
      todo = new mongoose.model('Todo', {todo: 'Clean the fish', userId: user.id});
      todo.save();
    });
```


## Use relationship

```javascript
    
    // Get the whole user object
    todo.fetchOwner(function(err, owner){
        console.log(owner); //=> {_id: '234972308a0ca0aeef70', name: 'Fred', email: 'fred@flintstones.com'}
    });

    // Select parts of a user
    todo.fetchOwner('name', function(err, owner){
        console.log(owner); //=> {_id: '234972308a0ca0aeef70', name: 'Fred'}
    });

```

